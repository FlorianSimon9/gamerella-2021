using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootSequence : ShootController
{
    [Serializable]
    public class Sequence
    {
        public ShootController shootController;
        public float startTime;
        public float duration;

        bool playing = false;
        float savedDuration = 0f;

        public float SavedDuration
        {
            get { return savedDuration; }
            set { savedDuration = value; }
        }

        public bool IsPlaying
        {
            get { return playing; }
            set { playing = value; }
        }
    }

    [Header("Shoot Sequence")]
    public List<Sequence> listShootSequence = new List<Sequence>();

    public bool shootCalled = false;

    int previousIndex = 0;
    int nextIndex = 0;

    int currentIndex = 0;

    float savedTime = 0f;

    float maximumTime = 0f;

    public float GetMaximumTime()
    {
        float max = 0f;

        for (int i = 0; i < listShootSequence.Count; i++)
        {
            var sequence = listShootSequence[i];
            float time = sequence.duration + sequence.startTime;
            if (time >= max)
            {
                max = time;
            }
        }

        return max;
    }

    public override void StartShooting()
    {
        base.StartShooting();

        shootCalled = true;

        maximumTime = GetMaximumTime();
    }

    public void FixedUpdate()
    {    
        if(shootCalled)
        {
            savedTime += Time.fixedDeltaTime;

            for(int i = 0; i < listShootSequence.Count; i++)
            {
                var sequence = listShootSequence[i];
                if (!sequence.IsPlaying && i >= currentIndex)
                {
                    if (sequence.startTime <= savedTime)
                    {
                        sequence.shootController.StartShooting();
                        sequence.shootController.enabled = true;
                        sequence.IsPlaying = true;
                        sequence.SavedDuration = 0;
                        currentIndex++;
                    }
                }
                else if(sequence.IsPlaying)
                {
                    sequence.SavedDuration += Time.deltaTime;
                    if(sequence.SavedDuration >= sequence.duration)
                    {
                        sequence.shootController.enabled = false;
                        sequence.IsPlaying = false;
                    }
                }

                if(maximumTime <= savedTime)
                {
                    savedTime = 0.0f;
                    currentIndex = 0;
                }

                
            }
        }
    }
}
