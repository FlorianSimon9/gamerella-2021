using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public Rigidbody2D rigidbody;


    PoolBulletController.BulletType _type;

    public void LaunchBullet(PoolBulletController.BulletType type, float speed, Vector3 direction, int layerValue, bool isLaunchByPlayer = false)
    {
        _type = type;
        Vector3 velocity = speed * direction;

        if (layerValue == 7)
        {
            this.gameObject.layer = 9;
        }
        else
        {
            this.gameObject.layer = 10;
        }
        rigidbody.velocity = velocity;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        PoolBulletController.instance.DisableBulletByType(this.gameObject, _type);
    }
}
