using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml;
using System.IO;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManager : MonoBehaviour
{

    public int PlayerLife = 3;

    public int nextLevelToLoad = 1;

    public GameObject player;
    public DateScroller dateScroller;
    private MusicPlayer musicPlayer;
    public AudioClip hitClip;
    public AudioSource hitSource;
    public Canvas gameOverUi;
    public Canvas congratsScreenUi;
    public Camera cameraTransition;
    public float timeTransitioCamera;

    public Text HpText;

    private CameraCoordinatesCache coordinate;
    private GameObjectHierarchyDatabase goHierachyScript;
    private BeautifyEffect.Beautify beautify;


    private int valueMoveCamera = 200;
    private bool needPressSpace = false;

    void Awake() 
    {
        this.musicPlayer = GetComponentInChildren<MusicPlayer>();
        beautify = GetComponentInChildren<BeautifyEffect.Beautify>();
        beautify.enabled = false;

        coordinate = this.GetComponent<CameraCoordinatesCache>();

        Vector3 cameraMainPosition = Camera.main.transform.position;
        cameraTransition.gameObject.SetActive(false);
        cameraTransition.transform.position = new Vector3(cameraMainPosition.x + valueMoveCamera, cameraMainPosition.y, cameraMainPosition.z);
    }

    void Start()
    {
        ReadXML();

        HpText.text = "x " + PlayerLife.ToString();


        bool alreadyLoaded = false;

        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            Scene scene = SceneManager.GetSceneAt(i);

            if (scene.buildIndex == nextLevelToLoad)
            {
                nextLevelToLoad++;
                alreadyLoaded = true;
            }
        }

        if(!alreadyLoaded)
        {
            GoToNextScene();
        }
        else
        {
            this.musicPlayer.StartMusic(this.nextLevelToLoad - 1);

            if (nextLevelToLoad - 1 == 3)
            {
                beautify.enabled = true;
            }

            var poolBullet = GameObject.Find("/PoolBullet");
            goHierachyScript = poolBullet.GetComponent<GameObjectHierarchyDatabase>();

            dateScroller.gameObject.SetActive(true);
            this.dateScroller.StartCoroutine("PlayAnimation", this.nextLevelToLoad - 1);

            player = GameObject.FindGameObjectsWithTag("Player")[0];
            
            var script = player.GetComponent<HPController>();
            script.hp = PlayerLife;
            script.deadEvent.AddListener(GameOver);
            script.hitEvent.AddListener(PlayerHit);
        }

        
    }

    void ReadXML()
    {
        var asset = (TextAsset)Resources.Load("player");
        if (asset != null)
        {
            var reader = new XmlTextReader(new StringReader(asset.text));
            while (reader.Read())
            {
                if (reader.Name == "playerLife")
                {
                    string value = reader.GetAttribute("hp");
                    PlayerLife = Int32.Parse(value);
                    break;
                }
            }
        }
    }

    public void GoToNextScene()
    {
        if (this.nextLevelToLoad == 4)
        {
            this.Congrats();
            return;
        }

        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator CameraTransition()
    {
        Vector3 cameraMainPosition = Camera.main.transform.position;
        cameraTransition.transform.position = new Vector3(cameraMainPosition.x + valueMoveCamera, cameraMainPosition.y, cameraMainPosition.z);
        cameraTransition.gameObject.SetActive(true);
        var rectCamera = cameraTransition.rect;
        rectCamera.x = 0;

        cameraTransition.rect = rectCamera;
        goHierachyScript.MoveToHorizontal(200f);
        
        yield return null;

        float savedTime = 0f;

        float posX = cameraTransition.transform.position.x;

        while (savedTime / timeTransitioCamera < 1f)
        {
            float value = Mathf.Lerp( 0f, 1f, savedTime / timeTransitioCamera);

            float posXMoved = Mathf.Lerp(posX, posX + coordinate.cameraWidth / 2f, savedTime / timeTransitioCamera);
            cameraTransition.transform.position = new Vector3(posXMoved, cameraMainPosition.y, cameraMainPosition.z);

            rectCamera.x = value;
            cameraTransition.rect = rectCamera;

            savedTime += Time.deltaTime;
            yield return null;
        }

        cameraTransition.gameObject.SetActive(false);
        StartCoroutine(UnloadYourAsyncScene());
    }

    IEnumerator UnloadYourAsyncScene()
    {

        AsyncOperation asyncLoad = SceneManager.UnloadSceneAsync(nextLevelToLoad-2);

        // Wait until the asynchronous scene fully unload
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

    }

    void Update()
    {
        if(needPressSpace)
        {
            if (Input.GetKey("space"))
                SceneManager.LoadScene(0,LoadSceneMode.Single);
        }
    }

    public void PlayerHit()
    {
        var script = player.GetComponent<HPController>();
        PlayerLife = script.hp;

        HpText.text = "x " + PlayerLife.ToString();
    }


    public void GameOver()
    {
        GameManager.instance.gameOverUi.gameObject.SetActive(true);
        needPressSpace = true;
    }


    public void Congrats()
    {
        GameManager.instance.congratsScreenUi.gameObject.SetActive(true);
        needPressSpace = true;
    }

    public void PlayHitSound()
    {
        GameManager.instance.hitSource.clip = GameManager.instance.hitClip;

        GameManager.instance.hitSource.volume = 0.2f;

        GameManager.instance.hitSource.Play();
    }

    IEnumerator LoadYourAsyncScene()
    {
        this.musicPlayer.StartMusic(this.nextLevelToLoad);

        if (nextLevelToLoad == 3)
        {
            beautify.enabled = true;
        }

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(nextLevelToLoad, LoadSceneMode.Additive);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        this.dateScroller.StartCoroutine("PlayAnimation", this.nextLevelToLoad);

        Vector3 oldPlayerPosition = Vector3.zero;

        if (player != null)
        {
            oldPlayerPosition = player.transform.position;
        }

        var poolBullet = GameObject.Find("/PoolBullet");
        goHierachyScript = poolBullet.GetComponent<GameObjectHierarchyDatabase>();

        GameObject[] goArray = GameObject.FindGameObjectsWithTag("Player");

        foreach( var go in goArray)
        {
            if (go.scene.buildIndex == nextLevelToLoad)
                player = go;
        }

        if (player != null)
        {
            player.transform.position = new Vector3(oldPlayerPosition.x, oldPlayerPosition.y, player.transform.position.z);
        }

        var script = player.GetComponent<HPController>();
        script.hp = PlayerLife;
        script.deadEvent.AddListener( GameOver );
        script.hitEvent.AddListener(PlayerHit);

        nextLevelToLoad++;

        if (nextLevelToLoad - 1 > 1)
        {
            StartCoroutine(CameraTransition());
        }
    }


    private static GameManager s_instance;
    private static bool s_instanceCreated;
    private static bool s_isQuitting;

    public static GameManager instance
    {
        get
        {
            if (s_isQuitting || Application.isPlaying == false)
            {
                return null;
            }

            if (s_instanceCreated == false)
            {
                CreateInstance();
            }

            return s_instance;
        }
    }


    public static void CreateInstance()
    {
        s_instanceCreated = true;

        if (s_instance == null)
        {
            s_instance = FindObjectOfType<GameManager>();

            if (s_instance == null)
            {
                Debug.LogError(typeof(GameManager).Name + " No instance.");
            }
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var gameManager = target as GameManager;

        if (GUILayout.Button("Simulate Next Scene"))
        {
            gameManager.GoToNextScene();
        }
    }
}
#endif
