using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendForceVectorGrid : MonoBehaviour
{
    public Color color;
    public bool isEnnemy = true;

    VectorGrid script;

    // Start is called before the first frame update
    void Start()
    {
        var obj = GameObject.FindGameObjectWithTag("VectorGrid");
        script = obj.GetComponent<VectorGrid>();

        var hp = this.GetComponent<HPController>();
        if(isEnnemy)
        {
            hp.hitEvent.AddListener(ForceGrid);
            hp.deadEvent.AddListener(DeadGrid);
        }
        else
        {
            hp.hitEvent.AddListener(ForceGrid);
        }
        
        
    }

    private void DeadGrid()
    {
        script.AddGridForce(this.transform.position, 5f, 4f, color, true);
    }

    private void ForceGrid()
    {
        script.AddGridForce(this.transform.position, 3f, 2f, color, true);
    }

    
}
