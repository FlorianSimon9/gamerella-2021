using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollBackground : MonoBehaviour
{
    public float speed = 3f;

    public Material material;


    // Update is called once per frame
    void Update()
    {
        material.mainTextureOffset = new Vector2(Time.time * speed, 0);
    }
}
