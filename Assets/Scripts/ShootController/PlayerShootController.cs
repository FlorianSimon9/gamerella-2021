using UnityEngine;

public class PlayerShootController: ShootController {
    public AudioClip shotAudio;

    private AudioSource shotAudioSource;

    void Awake() {
        this.shotAudioSource = this.gameObject.AddComponent<AudioSource>();

        this.shotAudioSource.clip = this.shotAudio;

        this.shotAudioSource.volume = 0.2f;
    }

    protected override void Update() {
        base.Update();

        if ((Input.GetKey("space") || (0.9f <= Input.GetAxis("TriggerXbox")))  && CanShoot)
        {
            StartShooting();
        }
    }

    public override void StartShooting() {
        base.StartShooting();

        var  go = GetBullet();

        go.LaunchBullet(type , velocity, Vector3.right, this.gameObject.layer);

        this.shotAudioSource.Play();
    }
}
