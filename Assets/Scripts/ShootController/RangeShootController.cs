using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeShootController : ShootController
{

    [Header("Range Shoot Controller")]
    
    public int wayNum = 5;

    public int numberLine = 1;

    public float centerAngle = 180f;
    
    public float betweenAngle = 10f;

    

    public float nextLineDelay = 0.1f;


    public bool shootCalled = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (shootCalled && CanShoot)
        {
            StartShooting();
        }
    }

    public override void StartShooting()
    {
        base.StartShooting();

        shootCalled = false;

        StartCoroutine(ShootSequence());
    }

    IEnumerator ShootSequence()
    {
        float baseAngle = wayNum % 2 == 0 ? centerAngle - (betweenAngle / 2f) : centerAngle;

        for (int i = 0; i < numberLine; i++)
        {
            for (int j = 0; j < wayNum; j++)
            {
                var go = GetBullet();

                float num = j % 2 == 0 ? 1f : -1f;

                int val = j;
                if (j > 1)
                {
                    val = j % 2 == 0 ? j / 2 : (j + 1) / 2;
                }

                float angleZ = baseAngle + val * betweenAngle * num;

                Vector3 direction = new Vector3(Mathf.Cos(angleZ * Mathf.Deg2Rad), Mathf.Sin(angleZ * Mathf.Deg2Rad), 0);

                go.LaunchBullet(type, velocity, direction , this.gameObject.layer);

             

            }

            yield return new WaitForSeconds(nextLineDelay);
        }
        ResetTimeSaved();
        shootCalled = true;
    }

}
