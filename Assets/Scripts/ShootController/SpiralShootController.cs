using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralShootController : ShootController
{

    [Header("Range Shoot Controller")]

    public float startAngle = 0f;

    public int spiralShoot = 1;

    public bool moveLeft = false;

    public int numberBullet = 5;
    
    public float betweenAngle = 10f;

    

    public float nextShootDelay = 0.1f;


    public bool shootCalled = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (shootCalled && CanShoot)
        {
            StartShooting();
        }
    }

    public override void StartShooting()
    {
        base.StartShooting();

        shootCalled = false;

        StartCoroutine(ShootSequence());
    }

    IEnumerator ShootSequence()
    {
        float spiralWayShiftAngle = 360f / spiralShoot;

        float directionShoot = moveLeft == true ? 1f : -1f;

        for (int j = 0; j < numberBullet; j++)
        {

            for (int i = 0; i < spiralShoot; i++)
            {

                var go = GetBullet();
                float angleZ = startAngle + spiralWayShiftAngle * i + directionShoot* j * betweenAngle;

                Vector3 direction = new Vector3(Mathf.Cos(angleZ * Mathf.Deg2Rad), Mathf.Sin(angleZ * Mathf.Deg2Rad), 0);

                go.LaunchBullet(type, velocity, direction, this.gameObject.layer);             
            }

            yield return new WaitForSeconds(nextShootDelay);
        }


        ResetTimeSaved();
        shootCalled = true;
    }

}
