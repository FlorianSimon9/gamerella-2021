using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMShootController : ShootController
{

    [Header("Aim Shoot Controller")]
    
    public int numberBullet = 5;
 
    public float betweenAngle = 10f;

    public float speedMax = 0.0f;

    public float delayShoot = 0.2f;


    public bool shootCalled = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (shootCalled && CanShoot)
        {
            StartShooting();
        }
    }

    public override void StartShooting()
    {
        base.StartShooting();

        shootCalled = false;

        StartCoroutine(ShootSequence());
    }

    IEnumerator ShootSequence()
    {
        Vector3 positionPlayer = GameManager.instance.player.transform.position;

        Vector2 vector2Player = new Vector2(positionPlayer.x, positionPlayer.y);
        Vector2 vector2Bullet = new Vector2(transform.position.x, transform.position.y);
        Vector2 diff = vector2Bullet - vector2Player;
        diff.Normalize();

        float baseAngle = (Mathf.Atan2(diff.y, diff.x) + Mathf.PI) * Mathf.Rad2Deg;

        for (int i = 0; i < numberBullet; i++)
        {
            var go = GetBullet();

            float angleZ = Random.Range(baseAngle - betweenAngle, baseAngle + betweenAngle);

            Vector3 direction = new Vector3(Mathf.Cos(angleZ * Mathf.Deg2Rad), Mathf.Sin(angleZ * Mathf.Deg2Rad), 0);

            float speedBullet = velocity;
            if (speedMax != 0f)
            {
                speedBullet = Random.Range(velocity, speedMax);
            }

            go.LaunchBullet(type, speedBullet, direction , this.gameObject.layer);

            
        }

        yield return null;

        ResetTimeSaved();
        shootCalled = true;
    }

}
