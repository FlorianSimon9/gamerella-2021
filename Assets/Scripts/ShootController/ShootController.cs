using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{

    [Header("Shoot Controller Settings")]

    public PoolBulletController.BulletType type;

    public Vector3 offsetShoot = Vector3.zero;

    public float timeBetweenShoot = 0.5f;

    public float velocity = 20f;

    bool canShoot = false;
    float timeSaved = 0f;


    public bool CanShoot
    {
        get { return canShoot; }
    }

    protected void ResetTimeSaved()
    {
        timeSaved = 0f;
        canShoot = false;
    }

    private void Awake()
    {
        timeSaved = timeBetweenShoot + 1f;
    }

    protected virtual void Update()
    {
        timeSaved += Time.deltaTime;

        if(timeSaved >= timeBetweenShoot && !canShoot)
        {
            canShoot = true;
        }
    }


    public virtual void StartShooting()
    {
        canShoot = false;
        timeSaved = 0f;
    }


    protected BulletController GetBullet()
    {
        var go = PoolBulletController.instance.GetBulletByType(type);
        go.transform.position = this.transform.position + offsetShoot;

        var bullet = go.GetComponent<BulletController>();
        return bullet;
    }

}
