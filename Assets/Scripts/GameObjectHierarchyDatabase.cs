using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectHierarchyDatabase : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> gameObjectList = new List<GameObject>();

    public void MoveToHorizontal(float xPos)
    {
        foreach( var go in gameObjectList)
        {
            go.transform.position = new Vector3(go.transform.position.x + xPos, go.transform.position.y, go.transform.position.z);
        }
    }
}
