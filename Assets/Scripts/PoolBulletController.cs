using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


public class PoolBulletController : MonoBehaviour
{
    private static PoolBulletController s_instance;
    private static bool s_instanceCreated;
    private static bool s_isQuitting;

    public static PoolBulletController instance
    {
        get
        {
            if (s_isQuitting || Application.isPlaying == false)
            {
                return null;
            }

            if (s_instanceCreated == false)
            {
                CreateInstance();
            }

            return s_instance;
        }
    }

    private  void OnDestroy()
    {
        if (this == s_instance)
        {
            s_instance = null;
            s_instanceCreated = false;
        }
    }


    public enum BulletType
    {
        Type1 = 0,
        Type2 = 1,
        Type3 = 2,
        Type4 = 3,
        Type5 = 4
    }

    [Serializable]
    public class BulletsListsPool
    {
        [SerializeField]
        public List<GameObject> TypeActive = new List<GameObject>();

        [SerializeField]
        public List<GameObject> TypeInactive = new List<GameObject>();
    }

    [SerializeField]
    public BulletsListsPool[] bulletsListsPool;


    [Header("Bullets Create Feature")]
    public BulletType type;
    public GameObject Bullet;
    public int nbrToCreate;


    public static void CreateInstance()
    {
        s_instanceCreated = true;

        if (s_instance == null)
        {
            s_instance = FindObjectOfType<PoolBulletController>();

            if (s_instance == null)
            {
                Debug.LogError(typeof(PoolBulletController).Name + " No instance.");   
            }
        }
    }


    public void DisableBulletByType(GameObject go, BulletType type)
    {
        if(bulletsListsPool[(int)type].TypeActive.Contains(go))
        {
            bulletsListsPool[(int)type].TypeActive.Remove(go);

            go.SetActive(false);
            go.transform.position = new Vector3(100, 100, 0);

            bulletsListsPool[(int)type].TypeInactive.Add(go);
        }
        else
        {
            Debug.LogError("Does not contain bullet", go);
        }
    }


    public GameObject GetBulletByType(BulletType type)
    {
        var go = bulletsListsPool[(int)type].TypeInactive[0];
        bulletsListsPool[(int)type].TypeInactive.RemoveAt(0);

        bulletsListsPool[(int)type].TypeActive.Add(go);

        go.SetActive(true);

        return go;
    }

#if UNITY_EDITOR
    public void CleandBullet()
    {
        int childCount = this.transform.childCount;

        for(int i =0; i < childCount; i++)
        {
            UnityEngine.Object.DestroyImmediate(this.transform.GetChild(0).gameObject);
        }

        var values = Enum.GetValues(typeof(BulletType));

        foreach( var val in values)
        {
            bulletsListsPool[(int)val].TypeActive.Clear();
            bulletsListsPool[(int)val].TypeInactive.Clear();
        }

        
    }
#endif

#if UNITY_EDITOR
    public void BuildBullet()
    {
        for(int i = 0; i < nbrToCreate; i++)
        {
            GameObject go = (GameObject) PrefabUtility.InstantiatePrefab(Bullet, this.transform);

            go.transform.position = new Vector3(100, 100, 0);
            go.SetActive(false);

            bulletsListsPool[(int)type].TypeInactive.Add(go);
        }
    }
#endif
}


#if UNITY_EDITOR

[CustomEditor(typeof(PoolBulletController))]
public class ObjectBuilderEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PoolBulletController myScript = (PoolBulletController)target;
        if (GUILayout.Button("Build Bullets"))
        {
            myScript.BuildBullet();
        }

        if (GUILayout.Button("Cleans Bullets"))
        {
            myScript.CleandBullet();
        }
    }
}

#endif
