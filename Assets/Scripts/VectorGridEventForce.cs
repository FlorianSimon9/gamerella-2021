using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorGridEventForce : MonoBehaviour
{
    public List<GameObject> Ennemies = new List<GameObject>();

    public float timeUpdate = 0.5f;
    float savedTime = 0.0f;
    VectorGrid vectorGrid;

    // Start is called before the first frame update
    void Start()
    {
        vectorGrid = this.GetComponent<VectorGrid>();
        var player = GameManager.instance.player;
        var script = player.GetComponent<HPController>();
        script.hitEvent.AddListener(PlayerHit);
    }

    private void PlayerHit()
    {
        if (GameManager.instance.player != null)
        {
            vectorGrid.AddGridForce(GameManager.instance.player.transform.position, 3f, 3f, Color.blue, true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        savedTime += Time.deltaTime;
        if (savedTime >= timeUpdate)
        {
            savedTime = 0f;

            GameObject[] EnnemiesArray = GameObject.FindGameObjectsWithTag("Ennemy");

            foreach (var enemyArray in EnnemiesArray)
            {
                bool contains = false;

                foreach (var enemy in Ennemies)
                {
                    if (enemy.GetInstanceID() == enemyArray.GetInstanceID())
                    {
                        contains = true;
                    }
                }

                if (!contains)
                {
                    Ennemies.Add(enemyArray);
                }
            }

            foreach (var enemy in Ennemies)
            {
                var script = enemy.GetComponent<HPController>();

                if (!script)
                {
                    if (enemy.name.Contains("Enemy1"))
                        script.deadEvent.AddListener(EnemyHitRed);
                }
            }
        }
    }

        private void EnemyHitRed()
        {
            if (GameManager.instance.player != null)
            {
                vectorGrid.AddGridForce(GameManager.instance.player.transform.position, 3f, 3f, Color.red, true);
            }
        }
    }

