using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class HPController : MonoBehaviour
{
    public int hp = 3;

    public LayerMask mask;

    public UnityEvent hitEvent;

    public UnityEvent deadEvent;

    public bool hit = false;
    public bool isGodModePlayer = false;
    private CharacterHit invincibility;

    private bool ContainLayer(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }

    void OnDamage(int layer) {
        var isImmune = this.invincibility != null && this.invincibility.enabled;

        if (!hit && ContainLayer(mask, layer) && !isImmune)
        {
            hp--;

            hit = true;

            hitEvent?.Invoke();

            if (hp <= 0)
            {
                deadEvent?.Invoke();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        OnDamage(other.gameObject.layer);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        OnDamage(other.gameObject.layer);
    }

    public void FixedUpdate()
    {
        if (hit)
            hit = false;
    }

    public void DestroyGameObject()
    {
        
            Destroy(this.gameObject);
        
    }

    void Start() {
        this.invincibility = GetComponent<CharacterHit>();
    }

    void Awake()
    {
        if (System.Environment.GetCommandLineArgs().Any(argument => argument == "--god-mode") && this.isGodModePlayer) {
            this.hp = 10000;
        }
    }
}
