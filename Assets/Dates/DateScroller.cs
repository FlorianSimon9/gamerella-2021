using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DateScroller: MonoBehaviour {
    public Text text;

    [Serializable]
    public struct DateStyle {
        public Font font;
        public string date;
        public Color color;
    }

    private DateStyle level;

    public DateStyle level1;
    public DateStyle level2;
    public DateStyle level3;

    private DateStyle[] levels;

    IEnumerator PlayAnimation(int levelNumber) {
        if (levelNumber < 1 || levelNumber > 3) {
            throw new UnityException("YO MAN TON LEVEL NUMBER IL SCHLINGUE DLA TEUB");
        }

        this.enabled = true;

        var animator = this.text.GetComponent<Animator>();

        animator.enabled = true;

        this.level = this.levels[levelNumber - 1];

        this.text.text = this.level.date;
        this.text.font = this.level.font;
        this.text.color = this.level.color;

        var state = animator.GetCurrentAnimatorStateInfo(0);

        animator.Play("Done", 0, 0.0f);

        yield return new WaitForSeconds(state.length * state.speed);
    }

    void Awake() {
        this.levels = new DateStyle[3] { this.level1, this.level2, this.level3 };
    }
}
