using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class Choregraphy: ScriptableObject {
    public enum EnemyType {
        Tabouret = 0,
        Bite = 1,
        Couteau = 2,

        Boss = 3
    }

    [Serializable]
    public struct Movement
    {
        public float duration;
        public Vector2 position;
    }

    [Serializable]
    public class EnemySpawn {
        public float spawnAfter;
        public float startShootingAfter;
        public EnemyType type = EnemyType.Bite;
        public int movementRestartKeyframeIndex = 0;

        public float timeOfApparitionReadOnly;

        public Movement[] movements;

        class IndexPair {
            public int from = 0;
            public int to = 1;
        }

        IEnumerable<IndexPair> GenerateIndices() {
            IndexPair pair = new IndexPair();

            while (true) {
                yield return pair;

                if (this.type != EnemyType.Boss && pair.to == this.movements.Length - 1) {
                    break;
                }

                pair.from = pair.to;

                pair.to = pair.to + 1 < this.movements.Length ? pair.to + 1 : this.movementRestartKeyframeIndex;
            }
        }

        public struct Transition {
            public Movement from;
            public Movement to;
            public float elapsedTimeInPercent;
        }

        public Transition? GetKeyframe(float elapsedTime) {
            float accumulatedTime = 0.0f;

            foreach (var indexPair in GenerateIndices()) {
                if (accumulatedTime + this.movements[indexPair.from].duration >= elapsedTime) {
                    Transition transition = new Transition();

                    transition.from = this.movements[indexPair.from];
                    transition.to = this.movements[indexPair.to];

                    transition.elapsedTimeInPercent = Mathf.Lerp(0, transition.from.duration, (elapsedTime - accumulatedTime) / transition.from.duration) / transition.from.duration;

                    return transition;
                }

                accumulatedTime += this.movements[indexPair.from].duration;
            }

            return null;
        }
    }

    public abstract class IndexInfo {}

    public class CurrentIndex: IndexInfo {
        public CurrentIndex(int index, float time, bool isLast) {
            this.isLast = isLast;
            this.index = index;
            this.time = time;
        }

        public int index;
        public float time;
        public bool isLast = false;
    }

    public class NotStartedYet: IndexInfo {}

    public EnemySpawn[] enemies;

    public IndexInfo GetIndex(float Δt) {
        if (this.enemies[0].spawnAfter > Δt) {
            return new NotStartedYet();
        }

        float accumulatedTime = this.enemies[0].spawnAfter;

        for (var i = 1; i < this.enemies.Length; i++) {
            if (accumulatedTime + this.enemies[i].spawnAfter >= Δt) {
                return new CurrentIndex(i - 1, accumulatedTime, false);
            }

            accumulatedTime += this.enemies[i].spawnAfter;
        }

        return new CurrentIndex(this.enemies.Length - 1, accumulatedTime, true);
    }

    void OnValidate() {
        var accumulatedTime = 0.0f;

        foreach (var spawn in this.enemies) {
            accumulatedTime += spawn.spawnAfter;

            spawn.timeOfApparitionReadOnly = accumulatedTime;
        }
    }
}
