using System.Collections;
using UnityEngine;

public class ChoregraphedAnimation: MonoBehaviour {
    private Choregraphy.EnemySpawn spawn;
    private float startTime;

    public void Configure(Choregraphy.EnemySpawn spawn, float delayCompensation) {
        this.spawn = spawn;
        this.startTime = Time.fixedTime - delayCompensation;
    }

    void Start() {
        this.transform.position = new Vector3(100.0f, 100.0f, 0.0f);

        if (this.spawn.movements.Length < 2) {
            throw new UnityException("We need at least two values for animations.");
        }

        StartCoroutine("ScheduleFire");
    }

    void AnimatePosition(CameraCoordinatesCache cameraCoordinates, Choregraphy.EnemySpawn.Transition transition) {
        var inProportionsFrom = transition.from.position / 100.0f;
        var inProportionsTo = transition.to.position / 100.0f;

        var adaptatedFrom = new Vector2(
            (inProportionsFrom.x * cameraCoordinates.cameraWidth) + cameraCoordinates.left,
            (inProportionsFrom.y * cameraCoordinates.cameraHeight) + cameraCoordinates.bottom
        );

        var adaptatedTo = new Vector2(
            (inProportionsTo.x * cameraCoordinates.cameraWidth) + cameraCoordinates.left,
            (inProportionsTo.y * cameraCoordinates.cameraHeight) + cameraCoordinates.bottom
        );

        var lerpedPosition = Vector2.Lerp(adaptatedFrom, adaptatedTo, transition.elapsedTimeInPercent);

        this.transform.position = new Vector3(lerpedPosition.x, lerpedPosition.y, 2.0f);
    }

    IEnumerator ScheduleFire() {
        yield return new WaitForSeconds(this.spawn.startShootingAfter);

        ShootController[] controllers = GetComponents<ShootController>();

        foreach(var controller in controllers)
        {
            controller.StartShooting();
        }
    }

    void Update() {
        var camera = Camera.main;
        var transition = this.spawn.GetKeyframe(Time.fixedTime - startTime);

        var cameraCoordinates = camera.GetComponent<CameraCoordinatesCache>();

        if (transition == null) {
            Destroy(this.gameObject);

            return;
        }
        
        AnimatePosition(cameraCoordinates, transition.Value);
    }
}
