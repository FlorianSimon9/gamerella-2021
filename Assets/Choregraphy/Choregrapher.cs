using System.Collections.Generic;
using UnityEngine;

public class Choregrapher: MonoBehaviour {
    public int firstEnemyIndex = 0;

    public Choregraphy choregraphy;
    public GameObject tabouret;
    public GameObject bite;
    public GameObject couteau;
    public GameObject boss;

    private GameObject[] enemyByType;

    private float startTime;
    private int? previousIndex;

    private List<GameObject> spawnedEnemies;

    void Awake() {
        this.enemyByType = new GameObject[4];

        if(firstEnemyIndex != 0)
        {
            previousIndex = firstEnemyIndex++;
        }

        this.enemyByType[(int) Choregraphy.EnemyType.Tabouret] = this.tabouret;
        this.enemyByType[(int) Choregraphy.EnemyType.Couteau] = this.couteau;
        this.enemyByType[(int) Choregraphy.EnemyType.Bite] = this.bite;
        this.enemyByType[(int) Choregraphy.EnemyType.Boss] = this.boss;
    }

    void Start() {
        this.startTime = Time.fixedTime;

        this.spawnedEnemies = new List<GameObject>();
    }

    void TriggerAnimation(Choregraphy.EnemySpawn spawn, float plannedTime, float timeSinceStart, int indexEnemy) {
        var instance = Instantiate(this.enemyByType[(int) spawn.type]);

        if (spawn.type == Choregraphy.EnemyType.Boss) {
            instance.GetComponent<HPController>().deadEvent.AddListener(GameManager.instance.GoToNextScene);
        }

        instance.name = indexEnemy.ToString() + "_" + instance.name; 

        instance.transform.SetParent(this.transform);

        float delayCompensation = timeSinceStart - plannedTime;

        instance.AddComponent<ChoregraphedAnimation>().Configure(spawn, delayCompensation);

        spawnedEnemies.Add(instance);
    }

    void Update() {

        var offset = this.firstEnemyIndex > 0 ? this.choregraphy.enemies[this.firstEnemyIndex].timeOfApparitionReadOnly : 0;
        var timeSinceStart = Time.fixedTime + offset - startTime;

        var indexInfo = this.choregraphy.GetIndex(timeSinceStart);

        if (indexInfo is Choregraphy.NotStartedYet) {
            return;
        }

        var validIndexInfo = (indexInfo as Choregraphy.CurrentIndex);

        if (this.previousIndex != null && this.previousIndex >= validIndexInfo.index) {
            return;
        }

        for (int i = (this.previousIndex ?? -1) + 1; i <= validIndexInfo.index; i++) {
            TriggerAnimation(this.choregraphy.enemies[i], validIndexInfo.time, timeSinceStart, i);
        }

        this.previousIndex = validIndexInfo.index;

        if (validIndexInfo.isLast) {
            this.enabled = false;
        }
    }

    void OnValidate() {
        if (this.firstEnemyIndex >= this.choregraphy.enemies.Length) {
            throw new UnityException("We don't have that many enemies");
        }

        if (!Debug.isDebugBuild && this.firstEnemyIndex != 0) {
            throw new UnityException("In release, you cannot have simulation offsets");
        }
    }
}
