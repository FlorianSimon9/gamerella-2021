using UnityEngine;

public class Boundaries: MonoBehaviour {
    private GameObject[] bounds;

    private static readonly string[] names = { "Left boundary", "Bottom boundary", "Top boundary", "Right boundary" };

    private const float irrelevantDimensionsSize = 1.0f;

    void Start() {
        this.bounds = new GameObject[4];

        var cameraCoordinates = Camera.main.GetComponent<CameraCoordinatesCache>();

        var verticalBoundaryDimensions = new Vector3(cameraCoordinates.cameraWidth + 1.0f, irrelevantDimensionsSize, 1.0f);
        var horizontalBoundaryDimensions = new Vector3(irrelevantDimensionsSize, cameraCoordinates.cameraHeight + 1.0f, 1.0f);

        var dimensions = new Vector3[4] {
            horizontalBoundaryDimensions, verticalBoundaryDimensions, verticalBoundaryDimensions, horizontalBoundaryDimensions  
        };

        var positions = new Vector3[4] {
            new Vector3(cameraCoordinates.left - irrelevantDimensionsSize / 2.0f, cameraCoordinates.transform.position.y, 0.0f),
            new Vector3(cameraCoordinates.transform.position.x, cameraCoordinates.bottom - irrelevantDimensionsSize / 2.0f, 0.0f),
            new Vector3(cameraCoordinates.transform.position.x, cameraCoordinates.top + irrelevantDimensionsSize / 2.0f, 0.0f),
            new Vector3(cameraCoordinates.right + irrelevantDimensionsSize / 2.0f, cameraCoordinates.transform.position.y, 0.0f),
        };

        for (int i = 0; i < 4; i++) {
            var gameObject = new GameObject();

            gameObject.name = Boundaries.names[i];

            gameObject.transform.SetParent(this.gameObject.transform);

            gameObject.transform.localScale = dimensions[i];
            gameObject.transform.position = positions[i];

            this.bounds[i] = gameObject;

            gameObject.AddComponent<BoxCollider2D>();
        }
    }
}
