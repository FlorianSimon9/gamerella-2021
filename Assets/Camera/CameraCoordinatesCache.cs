using UnityEngine;

public class CameraCoordinatesCache: MonoBehaviour {
    public float top;
    public float left;
    public float right;
    public float bottom;

    public float cameraWidth;
    public float cameraHeight;

    void Awake() {
        var camera = GetComponent<Camera>();

        AssertCameraCorrect(camera);

        this.cameraHeight = camera.orthographicSize * 2;
        this.cameraWidth  = cameraHeight * camera.aspect;
        
        this.left = this.transform.position.x - this.cameraWidth / 2;
        this.right = this.transform.position.x + this.cameraWidth / 2;
        this.top = this.transform.position.y + this.cameraHeight / 2;
        this.bottom = this.transform.position.y - this.cameraHeight / 2;
    }

    void AssertCameraCorrect(Camera camera) {
        if (!camera.orthographic) {
            throw new UnityException("YO MAN TA CAMERA ELLE DOIT ETRE ORTHOGRAPHIQUE");
        }
    }
}
