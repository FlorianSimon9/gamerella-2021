using UnityEngine;

public class ShmupCharacter: MonoBehaviour {
    // In units per second.
    public float speed = 40.0f;

    private Rigidbody2D rigidBody;

    void Start() {
        this.rigidBody = this.GetComponent<Rigidbody2D>();

        this.rigidBody.gravityScale = 0.0f;
    }

    void Update() {
        var direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        direction.Normalize();

        this.rigidBody.velocity = speed * direction;
    }
}
