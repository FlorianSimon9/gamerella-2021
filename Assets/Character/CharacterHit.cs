using System.Collections;
using UnityEngine;

public class CharacterHit: MonoBehaviour {
    private Renderer[] renderersToAnimate;
    private Collider2D characterCollider;
    public float duration = 3.0f;
    public float period = 0.2f;

    // Start is called before the first frame update
    void Awake() {
        this.renderersToAnimate = this.GetComponentsInChildren<Renderer>();
        this.characterCollider = this.GetComponent<Collider2D>();

        if (this.renderersToAnimate.Length == 0) {
            throw new UnityException("YO ENFLURE TON COMPOSANT IL DOIT AVOIR UN RENDERER!");
        }
    }

    private void OnEnable() {
        StartCoroutine(PlayAnimation());
    }

    private void ConfigureRenderers(bool disabled) {
        foreach (var renderer in this.renderersToAnimate) {
            renderer.enabled = !disabled;
        }
    }

    IEnumerator PlayAnimation() {
        var accumulatedΔt = 0.0f;
        var halfPeriod = this.period / 2.0f;

        var inactive = true;

        this.enabled = true;
        this.characterCollider.enabled = false;

        do {
            ConfigureRenderers(inactive);

            inactive = !inactive;

            yield return new WaitForSeconds(halfPeriod);

            accumulatedΔt += halfPeriod;
        } while (accumulatedΔt < duration);

        characterCollider.enabled = true;

        ConfigureRenderers(false);

        this.enabled = false;
    }
}
