
using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
    using UnityEditor;
#endif

public class MusicPlayer: MonoBehaviour {
    [HideInInspector]
    public MusicDatabase database;

    [HideInInspector]
    public MusicDatabase.Level level;

    public float duration = 2.0f;


    [HideInInspector]
    private AudioSource introSource;

    [HideInInspector]
    private AudioSource levelSource;

    [HideInInspector]
    private AudioSource bossLoopSource;

    void Awake() {
        this.introSource = this.gameObject.AddComponent<AudioSource>();
        this.levelSource = this.gameObject.AddComponent<AudioSource>();
        this.bossLoopSource = this.gameObject.AddComponent<AudioSource>();

        this.introSource.volume = 0.1f;
        this.levelSource.volume = 0.1f;
        this.bossLoopSource.volume = 0.1f;

        this.introSource.playOnAwake = false;
        this.levelSource.playOnAwake = false;
        this.bossLoopSource.playOnAwake = false;

        this.bossLoopSource.loop = true;
    }

    public void StartMusic(int levelNumber) {
        if (levelNumber < 1 || levelNumber > 3) {
            throw new UnityException("PAS BON TON LEVEL NUMBER");
        }

        this.database = GetComponent<MusicDatabase>();

        this.level = this.database.levels[levelNumber - 1]; 

        this.introSource.clip = this.level.intro;
        this.levelSource.clip = this.level.level;
        this.bossLoopSource.clip = this.level.bossLoop;

        this.introSource.Play();

        float accumulatedLength = this.level.intro.length;

        this.levelSource.PlayDelayed(accumulatedLength);

        accumulatedLength += this.level.level.length;

        this.bossLoopSource.PlayDelayed(accumulatedLength);

        this.bossLoopSource.loop = true;
    }

    private AudioSource GetCurrentlyPlayingAudioSource() {
        if (this.levelSource.timeSamples > 0) {
            return this.levelSource;
        }

        if (this.bossLoopSource.timeSamples > 0) {
            return this.bossLoopSource;
        }

        if (this.introSource.timeSamples > 0) {
            return this.introSource;
        }

        return null;
    }

    private IEnumerator DoFadeOut() {
        float currentTime = 0;

        while (currentTime < this.duration) {
            var source = GetCurrentlyPlayingAudioSource();

            if (source == null) {
                this.enabled = false;

                yield break;
            }

            source.volume = Mathf.Lerp(1.0f, 0.0f, currentTime / this.duration);

            yield return null;

            currentTime += Time.deltaTime;
        }
    }

    public void FadeOut() {
        StartCoroutine("DoFadeOut");
    }
}

#if UNITY_EDITOR
    [CustomEditor(typeof(MusicPlayer))]
    public class MusicPauseButton: Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            var player = target as MusicPlayer;

            if (GUILayout.Button("Simulate the end of the level"))
            {
                player.FadeOut();
            }
        }
    }
#endif
