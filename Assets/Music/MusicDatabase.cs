using System;
using UnityEngine;

public class MusicDatabase: MonoBehaviour {
    [Serializable]
    public struct Level {
        public AudioClip intro;
        public AudioClip level;
        public AudioClip bossLoop;
    }

    public Level level1;
    public Level level2;
    public Level level3;

    [HideInInspector]
    public Level[] levels;

    void Awake() {
        this.levels = new Level[3] { level1, level2, level3 };

        foreach (var level in this.levels) {
            // The boss intro is optional.
            if (level.intro == null | level.level == null || level.bossLoop == null) {
                throw new UnityException("EN FAIT ON DIRAIT QUE TA MUSIQUE EST PAS BONNE.");
            }
        }
    }
}
