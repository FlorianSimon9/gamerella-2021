using UnityEngine.UI;
using UnityEngine;

public class BackgroundTransitionAnimator: MonoBehaviour {
    public Color color2;
    public Color color3;

    public Image image;

    private Color[] transitions;
    private Color[] keyframes;

    public int levelNumber = 1;
    public float colorChangeDuration = 2.0f;
    public float fadeOutDuration = 1.0f;

    private float animationStart;

    void StartAnimation() {
        this.enabled = true;
        this.animationStart = Time.fixedTime;

        var startColor = new Color(this.image.color.r, this.image.color.g, this.image.color.b, 1.0f);

        var nextColors = new Color[3] { this.color2, this.color3, startColor };

        var finalHue = nextColors[this.levelNumber - 1];

        var intermediateColor = new Color(finalHue.r, finalHue.g, finalHue.b, 1.0f);

        var lastColor = new Color(finalHue.r, finalHue.g, finalHue.b, 0.0f);

        if (this.levelNumber == 3) {
            this.colorChangeDuration = 0;
        }

        this.keyframes = new Color[3] { startColor, intermediateColor, lastColor };

        this.image.color = startColor;
    }

    void Start() {
        this.image = GetComponent<Image>();

        StartAnimation();
    }

    void Update() {
        var Δt = Time.fixedTime - this.animationStart;

        var fadeOutStarted = Δt > this.colorChangeDuration;

        var firstKeyframeIndex = fadeOutStarted ? 1 : 0;
        var start = fadeOutStarted ? this.colorChangeDuration : 0;
        var keyframeDuration = fadeOutStarted ? this.fadeOutDuration : this.colorChangeDuration;

        var keyframeΔt = Δt - start;

        this.image.color = Color.Lerp(keyframes[firstKeyframeIndex], keyframes[firstKeyframeIndex + 1], keyframeΔt / keyframeDuration);

        if (Δt > this.fadeOutDuration + this.colorChangeDuration) {
            this.enabled = false;
        }
    }
}
